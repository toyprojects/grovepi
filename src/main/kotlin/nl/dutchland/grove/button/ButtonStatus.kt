package nl.dutchland.grove.button

enum class ButtonStatus {
    PRESSED,
    NOT_PRESSED
}