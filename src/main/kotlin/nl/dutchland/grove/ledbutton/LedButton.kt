package nl.dutchland.grove.ledbutton

import nl.dutchland.grove.button.Button
import nl.dutchland.grove.led.Led

interface LedButton : Button, Led {
    override fun stop()
}