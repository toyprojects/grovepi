package nl.dutchland.grove.events

inline class MuteEvent(val muteIsOn: Boolean) : Event