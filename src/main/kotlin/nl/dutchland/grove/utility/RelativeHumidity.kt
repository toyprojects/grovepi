package nl.dutchland.grove.utility

data class RelativeHumidity(val relativeHumidity : Fraction)