package nl.dutchland.grove.co2

interface AirConcentration {
    val partsPerMillion: Int
}