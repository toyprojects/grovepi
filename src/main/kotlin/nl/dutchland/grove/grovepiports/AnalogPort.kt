package nl.dutchland.grove.grovepiports

interface AnalogPort {
    val analogPin: Int
}