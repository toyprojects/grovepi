package nl.dutchland.grove.grovepiports

interface DigitalPort {
    val digitalPin: Int
}