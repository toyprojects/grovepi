package nl.dutchland.grove.grovepiports

interface SerialPort {
    val serialPort: String
}