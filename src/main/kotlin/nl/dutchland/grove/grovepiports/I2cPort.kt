package nl.dutchland.grove.grovepiports

interface I2cPort {
    val i2cDeviceNumber: Int
}