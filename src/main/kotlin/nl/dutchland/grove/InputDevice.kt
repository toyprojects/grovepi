package nl.dutchland.grove

interface InputDevice {
    fun start()
    fun stop()
}