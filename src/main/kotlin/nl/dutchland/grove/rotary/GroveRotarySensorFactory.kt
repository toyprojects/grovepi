package nl.dutchland.grove.rotary

import nl.dutchland.grove.grovepiports.AnalogPort
import com.github.yafna.raspberry.grovepi.GrovePi

class GroveRotarySensorFactory(private val grovePi: GrovePi) {
    fun on(port: AnalogPort, listener: RotaryChangedListener) : RotarySensor {
        return GroveRotarySensor(
                com.github.yafna.raspberry.grovepi.devices.GroveRotarySensor(grovePi, port.analogPin),
                listener)
    }
}