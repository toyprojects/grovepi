package nl.dutchland.grove

interface OutputDevice {
    fun stop()
}