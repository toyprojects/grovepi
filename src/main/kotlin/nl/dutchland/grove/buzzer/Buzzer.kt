package nl.dutchland.grove.buzzer

import nl.dutchland.grove.digitaloutput.DigitalOutputDevice

interface Buzzer : DigitalOutputDevice {
}