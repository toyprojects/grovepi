package nl.dutchland.grove.buzzer

import nl.dutchland.grove.digitaloutput.GrovePulseWidthModulationOutputDevice
import com.github.yafna.raspberry.grovepi.devices.GroveLed

internal class GroveBuzzer(groveLed : GroveLed)
    : AdjustableBuzzer, GrovePulseWidthModulationOutputDevice(groveLed)